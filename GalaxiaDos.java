package unam.mx.mandy;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GalaxiaDos extends AppCompatActivity {

   ImageButton imgb;
   TextView tv;
   int img = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galaxia_dos);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);


        imgb = (ImageButton) findViewById(R.id.imagenbtn);
        tv = (TextView)findViewById(R.id.txt_andy);

    }

    public void regresar(View view){
        Intent inte = new Intent(this, Galaxias.class);
        startActivity(inte);
        finish();

    }


    public void info(View view){
        AlertDialog.Builder buldier = new AlertDialog.Builder(this);
        buldier.setTitle(R.string.info1);
        buldier.setMessage("En ésta galaxia se muestran fotos que me mandaste alguna vez o fotos que yo te tomé," +
                " es una sección para admirarte, presiona las fotos para ver las demás.");
        buldier.setPositiveButton( "Entendido", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(GalaxiaDos.this, "¡Te quiero mucho!", Toast.LENGTH_SHORT).show();
            }
        });

        Dialog dialog = buldier.create();
        dialog.show();
    }






    public void click(View view){

        switch (img){
            case  1:
                imgb.setImageResource(R.mipmap.andy4);
                tv.setText(R.string.txt_imagen1_andy);
                img =2;
                break;

            case 2:
                imgb.setImageResource(R.mipmap.andy8);
                tv.setText(R.string.txt_imagen1_andy2);
                img =3;
                break;

                case 3:
                    imgb.setImageResource(R.mipmap.andy24);
                    tv.setText(R.string.txt_imagen1_andy4);
                    img =4;
                    break;

            case 4:
                imgb.setImageResource(R.mipmap.andy9);
                tv.setText(R.string.txt_imagen1_andy3);
                img =1;
                break;

        }

    }


}
