package unam.mx.mandy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText dia, mes, año;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        dia = (EditText)findViewById(R.id.txt_dia);
        mes = (EditText)findViewById(R.id.txt_mes);
        año = (EditText) findViewById(R.id.txt_año);

    }


    public void entrar(View view){
        String day, month, year;
        day= dia.getText().toString();
        month = mes.getText().toString();
        year = año.getText().toString();

        if(day.isEmpty()){
            Toast.makeText(this, "Andypiciosa, llena todo por favor.", Toast.LENGTH_SHORT).show();
        }else if(month.isEmpty() ){
            Toast.makeText(this, "Andypiciosa, llena todo por favor.", Toast.LENGTH_SHORT).show();
        } else if(year.isEmpty()){
            Toast.makeText(this, "Andypiciosa, llena todo por favor.", Toast.LENGTH_SHORT).show();
        }
        else{
            int campo1 = Integer.parseInt(day);
            int campo2 = Integer.parseInt(month);
            int campo3 = Integer.parseInt(year);
            if(campo1 == 21 && campo2 == 10 && campo3 == 2017 ){
                Toast.makeText(this, "Entrando a la otra Activity...", Toast.LENGTH_SHORT).show();
                Intent inte = new Intent(this, Galaxias.class);
                startActivity(inte);
                finish();
            }else{
                Toast.makeText(this, "No recuerdas :c", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
