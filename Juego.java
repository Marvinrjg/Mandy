package unam.mx.mandy;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.security.SecureRandom;

public class Juego extends AppCompatActivity {

    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn10, btn11, btn12, btn13, btn14, btn15, btn16;

    ImageView imagenFoto;

    int array[] = new int[16];
    int andy = 0;
    int boton = 0;

    boolean banderaAndy = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_juego);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        imagenFoto = (ImageView)findViewById(R.id.fotoiv);

        btn1 = (Button)findViewById(R.id.boton1);
        btn2 = (Button)findViewById(R.id.boton2);
        btn3 = (Button)findViewById(R.id.boton3);
        btn4 = (Button)findViewById(R.id.boton4);
        btn5 = (Button)findViewById(R.id.boton5);
        btn6 = (Button)findViewById(R.id.boton6);
        btn7 = (Button)findViewById(R.id.boton7);
        btn8 = (Button)findViewById(R.id.boton8);
        btn9 = (Button)findViewById(R.id.boton9);
        btn10 = (Button)findViewById(R.id.boton10);
        btn11 = (Button)findViewById(R.id.boton11);
        btn12 = (Button)findViewById(R.id.boton12);
        btn13 = (Button)findViewById(R.id.boton13);
        btn14 = (Button)findViewById(R.id.boton14);
        btn15 = (Button)findViewById(R.id.boton15);
        btn16 = (Button)findViewById(R.id.boton16);

        SecureRandom numero = new SecureRandom();


        for (int i =0; i< array.length; i++){
            array[i] = numero.nextInt(30);
        }


    }

    public void regresar(View view){
        Intent inte = new Intent(this, GalaxiaSiete.class);
        startActivity(inte);
        finish();
    }

    public void check(){
        SecureRandom numero = new SecureRandom();
        int num = numero.nextInt(6);

        if(banderaAndy){
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste tu carita hermosa, se mostrará la foto abajo.");
            buldier.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Que guapa!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();

            if(num == 0){
                imagenFoto.setImageResource(R.mipmap.andy32);
            }else if(num == 1){
                imagenFoto.setImageResource(R.mipmap.andy33);
            }else if(num == 2){
                imagenFoto.setImageResource(R.mipmap.andy34);
            }else if(num == 3){
                imagenFoto.setImageResource(R.mipmap.andy35);
            }else if(num == 4){
                imagenFoto.setImageResource(R.mipmap.andy37);
            }else if(num == 5){
                imagenFoto.setImageResource(R.mipmap.andy41);
            }

            btn1.setEnabled(false);
            btn2.setEnabled(false);
            btn3.setEnabled(false);
            btn4.setEnabled(false);
            btn5.setEnabled(false);
            btn6.setEnabled(false);
            btn7.setEnabled(false);
            btn8.setEnabled(false);
            btn9.setEnabled(false);
            btn10.setEnabled(false);
            btn11.setEnabled(false);
            btn12.setEnabled(false);
            btn13.setEnabled(false);
            btn14.setEnabled(false);
            btn15.setEnabled(false);
            btn16.setEnabled(false);

            banderaAndy = false;

        }
        else if (!banderaAndy){
            imagenFoto.setImageResource(R.mipmap.signo);
        }


        if( boton == 16 ){
            if(andy == 0){
                AlertDialog.Builder buldier = new AlertDialog.Builder(this);
                buldier.setTitle(R.string.info1);
                buldier.setMessage("El juego terminó y no salió el sol (tu carita bonita), reinicia el juego.");
                buldier.setPositiveButton("Entendido", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(Juego.this, "¡Te adoro!", Toast.LENGTH_SHORT).show();
                    }
                });

                Dialog dialog = buldier.create();
                dialog.show();
            }
        }

    }


    public void reiniciar(View view){
        Intent inte = new Intent(this, Juego.class);
        startActivity(inte);
        finish();
    }

    public void boton1(View view){
        boton++;

        if(array[0] == 1  || array[0] == 9  ){
            btn1.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn1.setEnabled(false);

        }
        else if(array[0] == 3){

            btn1.setBackgroundResource(R.mipmap.marvin);
            btn1.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }
        else{
            btn1.setBackgroundResource(R.mipmap.x);
            btn1.setEnabled(false);
        }

        check();
    }

    public void boton2(View view){
        boton++;

        if(array[1] == 1 || array[1] == 9 ){
            btn2.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn2.setEnabled(false);
        } else if(array[1] == 3){

            btn2.setBackgroundResource(R.mipmap.marvin);
            btn2.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn2.setBackgroundResource(R.mipmap.x);
            btn2.setEnabled(false);

        }

        check();
    }

    public void boton3(View view){
        boton++;

        if(array[2] == 1  || array[2] == 9 ){
            btn3.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn3.setEnabled(false);

        } else if(array[2] == 3){

            btn3.setBackgroundResource(R.mipmap.marvin);
            btn3.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn3.setBackgroundResource(R.mipmap.x);
            btn3.setEnabled(false);
        }

        check();
    }

    public void boton4(View view){
        boton++;

        if(array[3] == 1  || array[3] == 9 ){
            btn4.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn4.setEnabled(false);
        } else if(array[3] == 3){

            btn4.setBackgroundResource(R.mipmap.marvin);
            btn4.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn4.setBackgroundResource(R.mipmap.x);
            btn4.setEnabled(false);
        }

        check();
    }

    public void boton5(View view){
        boton++;

        if(array[4] == 1  || array[4] == 9 ){
            btn5.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn5.setEnabled(false);
        } else if(array[4] == 3){

            btn5.setBackgroundResource(R.mipmap.marvin);
            btn5.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn5.setBackgroundResource(R.mipmap.x);
            btn5.setEnabled(false);
        }
        check();

    }

    public void boton6(View view){
        boton++;

        if(array[5] == 1  || array[5] == 9 ){
            btn6.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn6.setEnabled(false);
        } else if(array[5] == 3){

            btn6.setBackgroundResource(R.mipmap.marvin);
            btn6.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn6.setBackgroundResource(R.mipmap.x);
            btn6.setEnabled(false);
        }
        check();

    }

    public void boton7(View view){
        boton++;

        if(array[6] == 1  || array[6] == 9 ){
            btn7.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn7.setEnabled(false);
        } else if(array[6] == 3){
            btn7.setBackgroundResource(R.mipmap.marvin);
            btn7.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();


        }else{
            btn7.setBackgroundResource(R.mipmap.x);
            btn7.setEnabled(false);
        }
        check();

    }

    public void boton8(View view){
        boton++;

        if(array[7] == 1  || array[7] == 9 ){
            btn8.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn8.setEnabled(false);
        } else if(array[7] == 3){

            btn8.setBackgroundResource(R.mipmap.marvin);
            btn8.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn8.setBackgroundResource(R.mipmap.x);
            btn8.setEnabled(false);
        }
        check();

    }

    public void boton9(View view){
        boton++;

        if(array[8] == 1  || array[8] == 9 ){
            btn9.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn9.setEnabled(false);
        } else if(array[8] == 3){

            btn9.setBackgroundResource(R.mipmap.marvin);
            btn9.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn9.setBackgroundResource(R.mipmap.x);
            btn9.setEnabled(false);

        }

        check();
    }

    public void boton10(View view){
        boton++;

        if(array[9] == 1  || array[9] == 9 ){
            btn10.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn10.setEnabled(false);
        } else if(array[9] == 3){

            btn10.setBackgroundResource(R.mipmap.marvin);
            btn10.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();

        }else{
            btn10.setBackgroundResource(R.mipmap.x);
            btn10.setEnabled(false);
        }

        check();
    }

    public void boton11(View view){
        boton++;

        if(array[10] == 1  || array[10] == 9 ){
            btn11.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn11.setEnabled(false);
        } else if(array[10] == 3){
            btn11.setBackgroundResource(R.mipmap.marvin);
            btn11.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();


        }else{
            btn11.setBackgroundResource(R.mipmap.x);
            btn11.setEnabled(false);
        }

        check();
    }

    public void boton12(View view){
        boton++;

        if(array[11] == 1  || array[11] == 9 ){
            btn12.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn12.setEnabled(false);
        } else if(array[11] == 3){

            btn12.setBackgroundResource(R.mipmap.marvin);
            btn12.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();
        }else{
            btn12.setBackgroundResource(R.mipmap.x);
            btn12.setEnabled(false);
        }
        check();

    }

    public void boton13(View view){
        boton++;


        if(array[12] == 1  || array[12] == 9 ){
            btn13.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn13.setEnabled(false);
        } else if(array[12] == 3){
            btn13.setBackgroundResource(R.mipmap.marvin);
            btn13.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();

        }else{
            btn13.setBackgroundResource(R.mipmap.x);
            btn13.setEnabled(false);
        }

        check();
    }

    public void boton14(View view){
        boton++;

        if(array[13] == 1  || array[13] == 9 )  {
            btn14.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn14.setEnabled(false);
        } else if(array[13] == 3){
            btn14.setBackgroundResource(R.mipmap.marvin);
            btn14.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();


        }else{
            btn14.setBackgroundResource(R.mipmap.x);
            btn14.setEnabled(false);
        }
        check();

    }

    public void boton15(View view){
        boton++;

        if(array[14] == 1  || array[14] == 9 ){
            btn15.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn15.setEnabled(false);
        } else if(array[14] == 3){
            btn15.setBackgroundResource(R.mipmap.marvin);
            btn15.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();

        }else{
            btn15.setBackgroundResource(R.mipmap.x);
            btn15.setEnabled(false);
        }

        check();
    }

    public void boton16(View view){
        boton++;

        if(array[15] == 1  || array[15] == 9 ){
            btn16.setBackgroundResource(R.mipmap.andy2juego);
            banderaAndy = true;
            andy++;
            btn16.setEnabled(false);
        } else if(array[15] == 3){

            btn16.setBackgroundResource(R.mipmap.marvin);
            btn16.setEnabled(false);
            AlertDialog.Builder buldier = new AlertDialog.Builder(this);
            buldier.setTitle(R.string.info1);
            buldier.setMessage("Encontraste un Marvin, sigue buscando para encontrar una Andy hermosa.");
            buldier.setPositiveButton("Seguir", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(Juego.this, "¡Un grillo!", Toast.LENGTH_SHORT).show();
                }
            });

            Dialog dialog = buldier.create();
            dialog.show();

        }else{
            btn16.setBackgroundResource(R.mipmap.x);
            btn16.setEnabled(false);
        }

        check();

    }

}
