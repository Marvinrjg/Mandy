package unam.mx.mandy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class GalaxiaSeis extends AppCompatActivity {

    EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galaxia_seis);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        et= (EditText)findViewById(R.id.txt_contraseña);
    }


    public void regresar(View view){
        Intent inte = new Intent(this, Galaxias.class);
        startActivity(inte);
        finish();

    }

    public void entrar(View view){
        String palabra = et.getText().toString();

        if(!palabra.isEmpty()){
            if(palabra.equalsIgnoreCase("Te amo")){
                Intent inte = new Intent(this, Amorcillo.class);
                startActivity(inte);
            }
            else {
                Toast.makeText(this, "Palabras equivocadas.", Toast.LENGTH_SHORT).show();
            }
        }else{
            Toast.makeText(this, "No hay ninguna frase...", Toast.LENGTH_SHORT).show();
        }

    }
}
