package unam.mx.mandy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class GalaxiaCuatro extends AppCompatActivity {

    int lol;
    Button btn_reproducir;
    ImageView iv;
    TextView tv;
    MediaPlayer mp[] = new MediaPlayer[6];
    String frases[] = new String[6];
    int posicion = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galaxia_cuatro);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        btn_reproducir = (Button)findViewById(R.id.btn_reproducir);
        iv = (ImageView)findViewById(R.id.imageView);
        tv = (TextView)findViewById(R.id.textView4);

       mp[0]= MediaPlayer.create(this, R.raw.adictoati);
        mp[1]= MediaPlayer.create(this, R.raw.angel);
        mp[2]= MediaPlayer.create(this, R.raw.coincidir);
        mp[3]= MediaPlayer.create(this, R.raw.masquetuamigo);
        mp[4]= MediaPlayer.create(this, R.raw.meenamora);
        mp[5]= MediaPlayer.create(this, R.raw.aprenderaquerer);




        frases[0] = "Texto Adicto";
        frases[1] = "Texto Angel";
        frases[2] = "Texto Coincidir";
        frases[3] = "Texto MasQue";
        frases[4] ="Texto Me anamora";
        frases[5] ="Texto Aprender";

    }


    @Override
    protected void onPause(){
        super.onPause();
        mp[posicion].stop();
        mp[0]= MediaPlayer.create(this, R.raw.adictoati);
        mp[1]= MediaPlayer.create(this, R.raw.angel);
        mp[2]= MediaPlayer.create(this, R.raw.coincidir);
        mp[3]= MediaPlayer.create(this, R.raw.masquetuamigo);
        mp[4]= MediaPlayer.create(this, R.raw.meenamora);
        mp[5]= MediaPlayer.create(this, R.raw.aprenderaquerer);
    }

    @Override
    protected void onStop(){
        super.onStop();
        mp[posicion].stop();
        mp[0]= MediaPlayer.create(this, R.raw.adictoati);
        mp[1]= MediaPlayer.create(this, R.raw.angel);
        mp[2]= MediaPlayer.create(this, R.raw.coincidir);
        mp[3]= MediaPlayer.create(this, R.raw.masquetuamigo);
        mp[4]= MediaPlayer.create(this, R.raw.meenamora);
        mp[5]= MediaPlayer.create(this, R.raw.aprenderaquerer);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        mp[posicion].stop();
        mp[0]= MediaPlayer.create(this, R.raw.adictoati);
        mp[1]= MediaPlayer.create(this, R.raw.angel);
        mp[2]= MediaPlayer.create(this, R.raw.coincidir);
        mp[3]= MediaPlayer.create(this, R.raw.masquetuamigo);
        mp[4]= MediaPlayer.create(this, R.raw.meenamora);
        mp[5]= MediaPlayer.create(this, R.raw.aprenderaquerer);
    }

    public void regresar(View view){
        Intent inte = new Intent(this, Galaxias.class);

        startActivity(inte);
        finish();


    }

    public void reporducirPausa(View view){
        if(!mp[posicion].isPlaying()){
         btn_reproducir.setBackgroundResource(R.mipmap.pausa);
         tv.setText(frases[posicion]);

         if(posicion == 0){
         iv.setImageResource(R.mipmap.andy6);
         }else if(posicion == 1){
             iv.setImageResource(R.mipmap.andy10);
         }else if(posicion == 2){
             iv.setImageResource(R.mipmap.andy11);
         }else  if(posicion == 3){
             iv.setImageResource(R.mipmap.andy12);
         }else if(posicion == 4){
             iv.setImageResource(R.mipmap.andy17);
         }else if(posicion == 5){
             iv.setImageResource(R.mipmap.andy15);
         }
         mp[posicion].start();
        }else{
            tv.setText(frases[posicion]);
            if(posicion == 0){
                iv.setImageResource(R.mipmap.andy6);
            }else if(posicion == 1){
                iv.setImageResource(R.mipmap.andy10);
            }else if(posicion == 2){
                iv.setImageResource(R.mipmap.andy11);
            }else  if(posicion == 3){
                iv.setImageResource(R.mipmap.andy12);
            }else if(posicion == 4){
                iv.setImageResource(R.mipmap.andy17);
            }else if(posicion == 5){
                iv.setImageResource(R.mipmap.andy15);
            }
            btn_reproducir.setBackgroundResource(R.mipmap.reproducir);
            mp[posicion].pause();
        }
    }


    public void stop(View view){
        if(mp[posicion] != null){
            mp[posicion].stop();
            posicion = 0;
            if(posicion == 0){
                iv.setImageResource(R.mipmap.andy6);
            }else if(posicion == 1){
                iv.setImageResource(R.mipmap.andy10);
            }else if(posicion == 2){
                iv.setImageResource(R.mipmap.andy11);
            }else  if(posicion == 3){
                iv.setImageResource(R.mipmap.andy12);
            }else if(posicion == 4){
                iv.setImageResource(R.mipmap.andy17);
            }else if(posicion == 5){
                iv.setImageResource(R.mipmap.andy15);
            }
            tv.setText(frases[posicion]);
            mp[0]= MediaPlayer.create(this, R.raw.adictoati);
            mp[1]= MediaPlayer.create(this, R.raw.angel);
            mp[2]= MediaPlayer.create(this, R.raw.coincidir);
            mp[3]= MediaPlayer.create(this, R.raw.masquetuamigo);
            mp[4]= MediaPlayer.create(this, R.raw.meenamora);
            mp[5]= MediaPlayer.create(this, R.raw.aprenderaquerer);
            btn_reproducir.setBackgroundResource(R.mipmap.reproducir);

        }
    }


    public void anterior(View view){
        if(posicion > 0 ){
            mp[posicion].stop();
            mp[0]= MediaPlayer.create(this, R.raw.adictoati);
            mp[1]= MediaPlayer.create(this, R.raw.angel);
            mp[2]= MediaPlayer.create(this, R.raw.coincidir);
            mp[3]= MediaPlayer.create(this, R.raw.masquetuamigo);
            mp[4]= MediaPlayer.create(this, R.raw.meenamora);
            mp[5]= MediaPlayer.create(this, R.raw.aprenderaquerer);
            posicion--;
            if(posicion == 0){
                iv.setImageResource(R.mipmap.andy6);
            }else if(posicion == 1){
                iv.setImageResource(R.mipmap.andy10);
            }else if(posicion == 2){
                iv.setImageResource(R.mipmap.andy11);
            }else  if(posicion == 3){
                iv.setImageResource(R.mipmap.andy12);
            }else if(posicion == 4){
                iv.setImageResource(R.mipmap.andy17);
            }else if(posicion == 5){
                iv.setImageResource(R.mipmap.andy15);
            }
            tv.setText(frases[posicion]);
            mp[posicion].start();
            btn_reproducir.setBackgroundResource(R.mipmap.pausa);
        }
        else{
            Toast.makeText(this, "Andy, no hay más canciones.", Toast.LENGTH_SHORT).show();
        }
    }


    public void siguiente(View view){
        if(posicion < mp.length-1){
            mp[posicion].stop();
            mp[0]= MediaPlayer.create(this, R.raw.adictoati);
            mp[1]= MediaPlayer.create(this, R.raw.angel);
            mp[2]= MediaPlayer.create(this, R.raw.coincidir);
            mp[3]= MediaPlayer.create(this, R.raw.masquetuamigo);
            mp[4]= MediaPlayer.create(this, R.raw.meenamora);
            mp[5]= MediaPlayer.create(this, R.raw.aprenderaquerer);
            posicion++;
            if(posicion == 0){
                iv.setImageResource(R.mipmap.andy6);
            }else if(posicion == 1){
                iv.setImageResource(R.mipmap.andy10);
            }else if(posicion == 2){
                iv.setImageResource(R.mipmap.andy11);
            }else  if(posicion == 3){
                iv.setImageResource(R.mipmap.andy12);
            }else if(posicion == 4){
                iv.setImageResource(R.mipmap.andy17);
            }else if(posicion == 5){
                iv.setImageResource(R.mipmap.andy15);
            }
            tv.setText(frases[posicion]);
            mp[posicion].start();
            btn_reproducir.setBackgroundResource(R.mipmap.pausa);
        }else{
            Toast.makeText(this, "Andy, no hay más canciones.", Toast.LENGTH_SHORT).show();
        }
    }
}
