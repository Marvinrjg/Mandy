package unam.mx.mandy;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class GalaxiaUno extends AppCompatActivity {


    private boolean ver1 = false ,ver2 = false, ver3 = false, ver4 = false, ver5 = false, ver6 = false, ver7 = false, ver8 = false, ver9 = false;
    TextView tv1,tv2,tv3,tv4,tv5,tv6,tv7,tv8,tv9;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galaxia_uno);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        tv1 = (TextView)findViewById(R.id.texto1);
        tv2 = (TextView)findViewById(R.id.texto2);
        tv3 = (TextView)findViewById(R.id.texto3);
        tv4 = (TextView)findViewById(R.id.texto4);
        tv5 = (TextView)findViewById(R.id.texto5);
        tv6 = (TextView)findViewById(R.id.texto6);
        tv7 = (TextView)findViewById(R.id.texto7);
        tv8 = (TextView)findViewById(R.id.texto8);
        tv9 = (TextView)findViewById(R.id.texto9);
    }

    public void regresar(View view){
        Intent inte = new Intent(this, Galaxias.class);
        startActivity(inte);
    }

    public void info(View view){
        AlertDialog.Builder buldier = new AlertDialog.Builder(this);
        buldier.setTitle(R.string.info1);
        buldier.setMessage("Son fotos muy chulas de nosotros. \nPuedes tocarlas para saber que pienso.");
        buldier.setPositiveButton(R.string.info1_1_1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(GalaxiaUno.this, "¡Te quiero!", Toast.LENGTH_SHORT).show();
            }
        });

        Dialog dialog = buldier.create();
        dialog.show();
    }

    public void boton1(View view){
        if(!ver1){
            tv1.setText(R.string.txt_imagen3);
            ver1 = true;
        }
        else {
            tv1.setText(R.string.txt_imagen3_vista);
            ver1 = false;
        }
    }
    public void boton2(View view){
        if(!ver2){
            tv2.setText(R.string.txt_imagen2);
            ver2 = true;
        }
        else {
            tv2.setText(R.string.txt_imagen2_vista);
            ver2 = false;
        }
    }
    public void boton3(View view){
        if(!ver3){
            tv3.setText(R.string.txt_imagen1);
            ver3 = true;
        }
        else {
            tv3.setText(R.string.txt_imagen1_vista);
            ver3 = false;
        }
    }
    public void boton4(View view){
        if(!ver4){
            tv4.setText(R.string.txt_imagen4);
            ver4 = true;
        }
        else {
            tv4.setText(R.string.txt_imagen4_vista);
            ver4 = false;
        }
    }
    public void boton5(View view){
        if(!ver5){
            tv5.setText(R.string.txt_imagen5);
            ver5 = true;
        }
        else {
            tv5.setText(R.string.txt_imagen5_vista);
            ver5 = false;
        }
    }
    public void boton6(View view){
        if(!ver6){
            tv6.setText(R.string.txt_imagen6);
            ver6 = true;
        }
        else {
            tv6.setText(R.string.txt_imagen6_vista);
            ver6 = false;
        }
    }
    public void boton7(View view){
        if(!ver7){
            tv7.setText(R.string.txt_imagen7);
            ver7 = true;
        }
        else {
            tv7.setText(R.string.txt_imagen7_vista);
            ver7 = false;
        }
    }
    public void boton8(View view){
        if(!ver8){
            tv8.setText(R.string.txt_imagen8);
            ver8 = true;
        }
        else {
            tv8.setText(R.string.txt_imagen8_vista);
            ver8 = false;
        }
    }
    public void boton9(View view){
        if(!ver9){
            tv9.setText(R.string.txt_imagen9);
            ver9 = true;
        }
        else {
            tv9.setText(R.string.txt_imagen9_vista);
            ver9 = false;
        }
    }
}
