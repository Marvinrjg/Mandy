package unam.mx.mandy;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.widget.VideoView;

public class Galaxias extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galaxias);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

    }

    public void galaxiaUno(View view){
       Intent inte = new Intent(this, GalaxiaUno.class);
       startActivity(inte);
       finish();

    }

    public void galaxiaDos(View view){
        Intent inte = new Intent(this, GalaxiaDos.class);
        startActivity(inte);
        finish();

    }

    public void galaxiaTres(View view){
        Intent inte = new Intent(this, GalaxiaTres.class);
        startActivity(inte);
        finish();

    }

    public void galaxiaCuatro(View view){
        Intent inte = new Intent(this, GalaxiaCuatro.class);
        startActivity(inte);
        finish();

    }

    public void galaxiaCinco(View view){
        Intent inte = new Intent(this, GalaxiaCinco.class);
        startActivity(inte);
        finish();

    }

    public void galaxiaSeis(View view){
        Intent inte = new Intent(this, GalaxiaSeis.class);
        startActivity(inte);
        finish();

    }

    public void galaxiaSiete(View view){
        Intent inte = new Intent(this, GalaxiaSiete.class);
        startActivity(inte);
        finish();

    }


    public void info(View view){
        AlertDialog.Builder buldier = new AlertDialog.Builder(this);
        buldier.setTitle(R.string.info1);
        buldier.setMessage(R.string.info1_1);
        buldier.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(Galaxias.this, "¡Te quiero!", Toast.LENGTH_SHORT).show();
            }
        });

        Dialog dialog = buldier.create();
        dialog.show();
    }



}
