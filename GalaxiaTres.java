package unam.mx.mandy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

public class GalaxiaTres extends AppCompatActivity {

    VideoView videov, videov2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_galaxia_tres);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        Uri path = Uri.parse("android.resource://unam.mx.mandy/"
                + R.raw.andyvideo);
        Uri path2 = Uri.parse("android.resource://unam.mx.mandy/"
                + R.raw.videoandy1);
        videov = (VideoView)findViewById(R.id.videoView2);
        videov.setMediaController((new MediaController(this)));
        videov.setVideoURI(path);
        videov.start();

        videov2 = (VideoView)findViewById(R.id.videoView);
        videov2.setMediaController((new MediaController(this)));
        videov2.setVideoURI(path2);
        videov2.start();
    }

    public void regresar(View view){
        Intent inte = new Intent(this, Galaxias.class);
        startActivity(inte);
        finish();

    }
}
